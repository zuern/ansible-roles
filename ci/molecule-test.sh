#!/bin/bash
cd "$(dirname "$0")"

echo Checking prerequisistes:
molecule --version || exit $?

echo Beginning the linting process
find ../ -mindepth 2 -maxdepth 2 -type d -name "molecule" -execdir pwd \;| while read -r role; do
	echo Checking directory: $role
	cd $role
	molecule lint && molecule syntax
done;

if [ $? -eq 0 ]
then
	echo Molecule test passed
else
	echo Molecule test failed
	exit 1
fi

