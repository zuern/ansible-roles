Docker-Compose
=========

Install all the dependencies required for docker-compose on Linux x86_64.

Requirements
------------

Ubuntu 18.04 or later

Role Variables
--------------

* `docker_compose_version`: The version of docker-compose to download.
* `docker_compose_checksums`: A map of docker-compose release version to the
  checksum of the binary download

Dependencies
------------


Example Playbook
----------------
```yaml
    - hosts: servers
      roles:
         - { role: docker-compose }
```

License
-------
GPLv3.0
