import os

import testinfra.utils.ansible_runner

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_svc_running(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled


def test_docker_compose_is_installed(host):
    dc = host.file("/usr/local/bin/docker-compose")
    assert dc.exists
    assert dc.is_file
    assert dc.mode == 0o0655
